#!/usr/bin/env bash

echo "SHUTDOWN SERVER...";

if [ ! -f ./server.pid ]; then
  echo "server.pid not found!"
  exit 1;
fi

pid=$(cat ./server.pid)
echo "KILL PROCESS WITH PID $pid";
kill -9 "$pid"
rm ./server.pid
echo "OK";