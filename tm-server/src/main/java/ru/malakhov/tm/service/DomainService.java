package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.*;
import ru.malakhov.tm.exception.empty.EmptyDomainException;

import java.util.List;

public final class DomainService implements IDomainService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void loadData(@Nullable final Domain domain) throws EmptyDomainException {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getUserService().merge(domain.getUsers());
        serviceLocator.getProjectService().merge(domain.getProjects());
        serviceLocator.getTaskService().merge(domain.getTasks());
        serviceLocator.getSessionService().merge(domain.getSessions());
    }

    @Override
    public void saveData(@Nullable final Domain domain) throws EmptyDomainException {
        if (domain == null) throw new EmptyDomainException();
        @NotNull final List<ProjectDto> projects = serviceLocator.getProjectService().findAllDto();
        domain.setProjects(projects);
        @NotNull final List<TaskDto> tasks = serviceLocator.getTaskService().findAllDto();
        domain.setTasks(tasks);
        @NotNull final List<UserDto> users = serviceLocator.getUserService().findAllDto();
        domain.setUsers(users);
        @NotNull final List<SessionDto> sessions = serviceLocator.getSessionService().findAllDto();
        domain.setSessions(sessions);
    }

}
