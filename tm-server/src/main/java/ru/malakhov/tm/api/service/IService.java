package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.dto.AbstractEntityDto;

import javax.persistence.EntityManager;
import java.util.Collection;

public interface IService<E extends AbstractEntityDto, R extends IRepository<E>> {

    @NotNull
    EntityManager getEntityManager();

    void persist(@Nullable final E entity);

    void persist(@Nullable final Collection<E> entities);

    void persist(@Nullable final E... entities);

    void merge(@Nullable final E entity);

    void merge(@Nullable final Collection<E> entities);

    void merge(@Nullable final E... entities);

    void removeOne(@Nullable E entity);

    @NotNull
    R getRepository();

}