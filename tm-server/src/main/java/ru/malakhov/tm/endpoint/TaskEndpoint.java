package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.ITaskEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearAllTask(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getTaskService().removeAll();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> getAllTaskList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findAllDto();
    }

    @NotNull
    @Override
    @WebMethod
    public Result createTask(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().create(session.getUserId(), name, description);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearTaskList(@WebParam(name = "session") @Nullable final SessionDto session) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().removeAllByUserId(session.getUserId());
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDto> getTaskList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllDtoByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeTaskById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeTaskByName(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneDtoById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneDtoByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskByName(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneDtoByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Result updateTaskById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().updateTaskById(
                    session.getUserId(),
                    id,
                    name,
                    description
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result updateTaskByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().updateTaskByIndex(
                    session.getUserId(),
                    index,
                    name,
                    description
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

}