package ru.malakhov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.exception.AbstractException;

public final class PropertyLoadException extends AbstractException {

    public PropertyLoadException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This value ``" + value + "`` is not number", cause);
    }

    public PropertyLoadException() {
        super("Error! Properties not loaded ...");
    }

}
