package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.util.HashUtil;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(nullable = false)
    private String login = "";

    @NotNull
    @Column(name = "password_hash", nullable = false)
    private String passwordHash = "";

    @NotNull
    @Column(nullable = false)
    private String email = "";

    @Nullable
    @Column(name = "first_name")
    private String firstName = "";

    @Nullable
    @Column(name = "last_name")
    private String lastName = "";

    @Nullable
    @Column(name = "middle_name")
    private String middleName = "";

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;

    @NotNull
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> sessions = new ArrayList<>();

    public User(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        this.login = login;
        this.email = email;
        this.passwordHash = HashUtil.salt(password);
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof User && super.equals(o)) {
            @NotNull final User user = (User) o;
            return Objects.equals(login, user.login)
                    && Objects.equals(passwordHash, user.passwordHash)
                    && Objects.equals(email, user.email);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, passwordHash, email);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Task> getSessions() {
        return sessions;
    }

    public void setSessions(List<Task> sessions) {
        this.sessions = sessions;
    }
}