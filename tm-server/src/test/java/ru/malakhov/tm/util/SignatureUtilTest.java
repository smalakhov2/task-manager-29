package ru.malakhov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.UnitCategory;
import ru.malakhov.tm.entity.Project;

@Category(UnitCategory.class)
public final class SignatureUtilTest {

    @NotNull
    private final String SALT = "sadasdasd";

    @NotNull
    private final Integer CYCLE = 345;

    @Test
    public void testSign() {
        @NotNull final Project project = new Project();
        @Nullable final String signature = SignatureUtil.sign(project, SALT, CYCLE);
        Assert.assertNotNull(signature);
        @Nullable final String resign = SignatureUtil.sign(project, SALT, CYCLE);
        Assert.assertEquals(signature, resign);
        project.setName("adasdasd");
        @Nullable final String updatedSignature = SignatureUtil.sign(project, SALT, CYCLE);
        Assert.assertNotEquals(signature, updatedSignature);

        @Nullable String incorrectSignature = SignatureUtil.sign(null, SALT, CYCLE);
        Assert.assertNull(incorrectSignature);
        incorrectSignature = SignatureUtil.sign(project, null, CYCLE);
        Assert.assertNull(incorrectSignature);
        incorrectSignature = SignatureUtil.sign(project, SALT, null);
        Assert.assertNull(incorrectSignature);
    }

}
