package ru.malakhov.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in system.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable SessionDto session = serviceLocator.getSessionEndpoint().openSession(login, password);
        if (session == null) {
            System.out.println("[ACCESS DENIED]");
            return;
        }
        serviceLocator.getPropertyService().setSession(session);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return false;
    }

}
