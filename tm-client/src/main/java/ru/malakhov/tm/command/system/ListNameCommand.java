package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.Set;

public final class ListNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Set<String> commandsName = serviceLocator.getCommandService().getCommandsName();
        for (final String name : commandsName) System.out.println(name);
    }

    @Override
    public boolean secure() {
        return false;
    }

}