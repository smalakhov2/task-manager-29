package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IPropertyRepository;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.endpoint.SessionDto;

public final class PropertyService implements IPropertyService {

    @NotNull
    private final IPropertyRepository propertyRepository;

    public PropertyService(@NotNull final IPropertyRepository propertyRepository) {
        this.propertyRepository = propertyRepository;
    }

    @Nullable
    @Override
    public SessionDto getSession() {
        return propertyRepository.getSession();
    }

    @Override
    public void setSession(@Nullable final SessionDto session) {
        propertyRepository.setSession(session);
    }

    @Override
    public boolean isAuth() {
        return getSession() != null;
    }

}