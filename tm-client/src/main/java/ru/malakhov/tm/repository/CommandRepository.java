package ru.malakhov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@NoArgsConstructor
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public void add(@NotNull final String name, @NotNull final AbstractCommand command) {
        commands.put(name, command);
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @NotNull
    @Override
    public Set<String> getCommandsName() {
        return commands.keySet();
    }

    @NotNull
    @Override
    public Set<String> getCommandsArg() {
        @NotNull final Set<String> args = new LinkedHashSet<>();
        for (@Nullable final AbstractCommand command : this.commands.values()) {
            if (command == null) continue;
            @Nullable final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            args.add(arg);
        }
        return args;
    }

}